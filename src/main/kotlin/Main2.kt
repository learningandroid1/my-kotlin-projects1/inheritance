

fun main() {
    val developer = Developer("Tony", 40)
    developer.writeCode()
    developer.getLevel()

    // сужающее приведение, Upcast, мы подняли\упростили Developer до Person
    // и теперь нам недоступны все фишки Developer
    val person: Person = developer

    val person2: Person = Developer("Tony", 40)
    val developer2: Developer? = person2 as? Developer
    developer2?.writeCode()
    println(developer2?.getLevel())
}
