class CppDeveloper: Developer {
    // разные конструкторы для разных случаев.
    constructor(name: String, age: Int): super(name, age) // experience будет браться по умолчанию из суперкласса
    constructor(name: String, age: Int, experience: Int): super(name, age, experience) // а тут как обычно, вводим экспириенс вручную.

    override fun writeCode(){
        super.writeCode()
        println("cpp code")
    }

    override fun getLevel(): String = when(experience) {
        0 -> "intern"
        in 1..3 -> "junior"
        in 4..6 -> "middle"
        in 6..8 -> "senior"
        else -> "lead"
    }
}
