fun main() {
    val person = Person("Vasya", 19)
    val driver = Driver("Petrovich", 47, 25)

    // у person нет поля "experience" и функции "drive", то бишь, из потомка.
    person.name

    // для вызова доступны все поля и функции, что и в суперклассе.
    driver.sleep()

    val ktDev = KotlinDeveloper("Alex", 25)
    val javaDev = KotlinDeveloper(name = "Ivan", age = 40, experience = 20)
    val cppDev = KotlinDeveloper(name = "Anna", age = 30, experience = 7)
    val developer = Developer(name = "megaMozg", age =100, experience = 100)
    val scalaDev = ScalaDeveloper(name = "Tanya", age = 32, experience = 5)

    ktDev.writeCode()
    println("ktDev level - ${ktDev.getLevel()}")

    javaDev.writeCode()
    println("javaDev level - ${javaDev.getLevel()}")

    cppDev.writeCode()
    cppDev.walk()
    println("cppDev level - ${cppDev.getLevel()}")

    developer.writeCode()
    println("developer level - ${developer.getLevel()}")

    println("javadev paradigm - ${javaDev.paradigm}")
    println("scaladev paradigm - ${scalaDev.paradigm}")
}