fun main() {
    println(genericFun(null))
    println(genericFun2(175))

    val object1 = Generic(5)

//    val object2 = Generic("String")

//    val person = Generic(Person("Nastya", 27))
//    person.setItem(Developer("Developer", 25))

    val first = Generic<Number>(5.0)
    val second = Generic<Number>(19.5)
    println(sum(first, second))
}

fun <T> genericFun(input: T): String {
    return input?.toString() ?: "Object is null"
}

fun <T> genericFun2(input: T): T {
    return input
}

fun sum(a: Generic<Number>, b: Generic<Number>): Int? {
    val first = a.item?.toInt() ?: return null
    val second = b.item?.toInt() ?: return null
    return first + second
    }