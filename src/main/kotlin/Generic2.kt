import java.util.*

class Generic2<out T: Number>(value: T) {
    private var item: T? = value
    val initializedDate = Date()

    fun getItem() = item
}