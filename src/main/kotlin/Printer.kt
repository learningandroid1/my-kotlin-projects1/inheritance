class Printer<T: Person> {
    fun print(person: T){
        println("Person - ${person.name}")
    }
}