fun main(){
   val robot1 = Robot(1)
   robot1.callToClient("Evgeny Petrovich")

   val manager1 = Manager("Olga", 67800)
   manager1.callToClient("Irina Nikolaevna")

   val clientWorkers = mutableListOf<ClientWorker>(robot1, manager1)
   for (clientWorker in clientWorkers)
       clientWorker.callToClient("Vasiliy Eduardovich")
}

interface ClientWorker{
    fun callToClient(clientName: String)
    fun emailToClinet(clientName: String)
}


class Robot(val id: Int): ClientWorker{
    override fun callToClient(clientName: String){
        println("Робот ${id}: звоню клиенту $clientName")
    }

    override fun emailToClinet(clientName: String) {
        println("Робот ${id}: пишу клиенту $clientName")
    }
}

abstract class Employee(val name: String, val salary: Int ){
    protected val projects: MutableList<String> = mutableListOf()

    abstract fun addProject(projectName: String)

    fun printAllProjects(){
        println("Проекты сотрудника $name: ${projects.joinToString()}")
    }
}

class Programmer(name: String, salary: Int): Employee(name, salary) {
    fun readArticle(articleName: String) {
        println("Программист ${name}: Начал читать статью $articleName")
    }

    override fun addProject(projectName: String) {
        projects.add(projectName)
        println("Программист ${name}: начал работу над проектом $projectName")
    }
}

class Manager(name: String, salary: Int): Employee(name, salary), ClientWorker{
    override fun callToClient(clientName: String){
        println("Менеджер ${name}: звоню клиенту $clientName")
    }

    override fun emailToClinet(clientName: String) {
        println("Менеджер ${name}: пишу клиенту $clientName")
    }

    override fun addProject(projectName: String){
        projects.add(projectName)
        println("Менеджер ${name}: передал работу над проектом $projectName программистам")
    }
}