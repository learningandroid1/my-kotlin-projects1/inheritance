class ScalaDeveloper: Developer {
    // разные конструкторы для разных случаев.
    constructor(name: String, age: Int): super(name, age) // experience будет браться по умолчанию из суперкласса
    constructor(name: String, age: Int, experience: Int): super(name, age, experience) // а тут как обычно, вводим экспириенс вручную.

    override val paradigm = "functional"
}
