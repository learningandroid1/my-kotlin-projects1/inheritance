class KotlinDeveloper: Developer {
    // разные конструкторы для разных случаев.
    constructor(name: String, age: Int): super(name, age) // experience будет браться по умолчанию из суперкласса
    constructor(name: String, age: Int, experience: Int): super(name, age, experience) // а тут как обычно, вводим экспириенс вручную.

    override fun writeCode(){
        println("kt developer writing kt code")
        super.getCoffeeBreak()
    }

    override fun getCoffeeBreak() = true
}
