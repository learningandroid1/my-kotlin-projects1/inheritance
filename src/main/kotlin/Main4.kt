fun main() {

    val first = Generic2<Double>(5.0)
    val second = Generic2<Int>(10)
    sum2(first, second)
}

fun <T> genericFun3(input: T): String {
    return input?.toString() ?: "Object is null"
}

fun <T> genericFun4(input: T): T {
    return input
}

fun sum2(a: Generic2<Number>, b: Generic2<Number>): Int? {
    val first = a.getItem()?.toInt() ?: return null
    val second = b.getItem()?.toInt() ?: return null
    return first + second
}

fun printDeveloper(printer: Printer<Developer>, developer: Developer){
    // some logic
    printer.print(developer)
}